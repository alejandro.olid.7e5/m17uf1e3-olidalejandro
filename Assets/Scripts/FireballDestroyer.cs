using UnityEngine;
using UnityEngine.SceneManagement;

public class FireballDestroyer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Destroyer")
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("Test");
        }
    }
}
