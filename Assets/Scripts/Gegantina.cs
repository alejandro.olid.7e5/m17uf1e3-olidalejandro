using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gegantina : MonoBehaviour
{
    PlayerManager playerManager;
    Vector3 initialScale;
    public int GegantinaCoolDown = 5;
    public bool gegantinaIsActive;
    public bool gegantinaCoolDownAvaible;
    public bool gegantinaCoolDownTimer;
    public bool gegantinaReset;
    public float time;


    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        time = 0;
        gegantinaIsActive = false;
        gegantinaCoolDownTimer = false;
        gegantinaCoolDownAvaible = true;
        gegantinaReset = false;
        initialScale = playerManager.Player.transform.localScale;
    }

    void Update()
    {
        if (Input.GetKeyDown("space") && gegantinaCoolDownAvaible)
        {
            print("GeGanTiNa AcTiVaTeD");
            gegantinaIsActive=true;
            gegantinaCoolDownAvaible = false;
        }

        if (gegantinaIsActive)
        {
            playerManager.Player.transform.localScale += new Vector3 (playerManager.Height / 15000, playerManager.Height / 15000, playerManager.Height / 15000);
            if (playerManager.Player.transform.localScale.x >= playerManager.Height*0.03)
            {
                gegantinaReset = true;
                gegantinaIsActive = false;
            }
        }

        if (gegantinaReset)
        {
            playerManager.Player.transform.localScale -= new Vector3(playerManager.Height / 4500, playerManager.Height / 4500, playerManager.Height / 4500);
            if (playerManager.Player.transform.localScale.x <= initialScale.x)
            {
                playerManager.Player.transform.localScale = initialScale;
                gegantinaReset = false;
                gegantinaCoolDownTimer = true;
            }
        }

        if (gegantinaCoolDownTimer)
        { 
            time += Time.deltaTime;
            if (time >= GegantinaCoolDown)
            {
                gegantinaCoolDownTimer = false;
                gegantinaCoolDownAvaible = true;
                time = 0;
            }
        }
    }


}
