using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public PlayerManager Instance;
    public GameObject Player;
    public Sprite RoleChoosenImage;
    public Sprite[] Sprites;
    public Sprite[] RoleSprites;
    public string PlayerName;
    public string RoleChoosen;
    public float Height = 150;
    public float Weight = 50;
    public float Velocity = 5;
    public float AnimationFramerate = 12;
    public int PatrolDistance = 10;
    public int SecondsWaiting = 4;
    public string[] RoleNames = { "Warrior", "Mage", "Healer" };


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
    }
}
