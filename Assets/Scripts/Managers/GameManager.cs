using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject Fireball;
    public int FireBallPositionY;
    public float MaxTimeBetweenFireBalls;
}
