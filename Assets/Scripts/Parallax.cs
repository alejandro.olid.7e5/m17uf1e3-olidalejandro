using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float _length, _startPos;
    public GameObject MyCamera;
    public float ParallaxEffect;
    private Vector3 previousCameraPosition;

    void Start()
    {
        previousCameraPosition = MyCamera.transform.position;
        _startPos = transform.position.x;
        _length = GetComponent<SpriteRenderer>().bounds.size.x;
    }
    void Update()
    {
        float dist = (MyCamera.transform.position.x - previousCameraPosition.x) * ParallaxEffect;
        float temp = MyCamera.transform.position.x * (1f - ParallaxEffect);


        transform.position = new Vector3(-1 * (_startPos + dist), transform.position.y, transform.position.z);

        if (temp > _startPos + _length)
        {
            transform.Translate(new Vector3(_length, 0, 0));
            _startPos += _length;

        }
        else if (temp < _startPos - _length)
        {
            transform.Translate(new Vector3(-_length, 0, 0));
        }
    }
}
