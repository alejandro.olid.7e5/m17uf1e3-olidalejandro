using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private GameObject player;
    private PlayerManager playerManager;
    public int jumpAmount = 100;
    public float gravityScale = 10;
    public float fallingGravityScale = 40;
    public float distanceToCheck = 2f;
    public bool isGrounded;
    public RaycastHit hit;
    SpriteRenderer spriteRenderer;
    Rigidbody2D rigidbody2D;

    private void Awake()
    {
        
    }


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerManager = FindObjectOfType<PlayerManager>();
        spriteRenderer = player.GetComponent<SpriteRenderer>();
        rigidbody2D = player.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float translation = Input.GetAxis("Horizontal") * playerManager.Velocity * (50 / playerManager.Weight) * Time.deltaTime;
        transform.Translate(translation, 0, 0);
        flipSprite(translation);
        createRayCastJump();
        jump();
    }

    private void flipSprite(float translation)
    {
        if (translation > 0)
            spriteRenderer.flipX = true;
        if (translation < 0)
            spriteRenderer.flipX = false;
    }

    void createRayCastJump()
    {
        var ray = Physics2D.Raycast(player.transform.position, Vector2.down, distanceToCheck);
        isGrounded = ray.collider.gameObject.tag == "Ground" ? true : false;
        Debug.DrawRay(player.transform.position, Vector3.down*distanceToCheck, Color.green);
    }

    void jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rigidbody2D.AddForce(Vector2.up * jumpAmount, ForceMode2D.Impulse);
        }
        if (rigidbody2D.velocity.y >= 0)
        {
            rigidbody2D.gravityScale = gravityScale;
        }
        else if (rigidbody2D.velocity.y < 0)
        {
            rigidbody2D.gravityScale = fallingGravityScale;
        }
    }

  /*  void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
    }
    void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
    }*/
}
