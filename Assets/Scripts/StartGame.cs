using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    PlayerManager playerManager;
    public TMP_InputField PlayerNameInput;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
    }

    public void StartGameScene()
    {
        SceneManager.LoadScene("GameStartScene");
    }

    public void SetPlayerName()
    {
        playerManager.PlayerName = PlayerNameInput.text;
    }

    public void SetWarriorRole()
    {
        playerManager.RoleChoosen = playerManager.RoleNames[0];
        playerManager.RoleChoosenImage = playerManager.Sprites[0];
    }

    public void SetMageRole()
    {
        playerManager.RoleChoosen = playerManager.RoleNames[1];
        playerManager.RoleChoosenImage = playerManager.Sprites[1];
    }

    public void SetHealerRole()
    {
        playerManager.RoleChoosen = playerManager.RoleNames[2];
        playerManager.RoleChoosenImage = playerManager.Sprites[2];
    }
}
