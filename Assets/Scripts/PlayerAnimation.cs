using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public GameObject player;
    public PlayerManager playerManager;
    public SpriteRenderer spriteRenderer;
    public float time;
    public float timeAnimation;
    private int i;

    void Start()
    {
        i = 0;
        playerManager = FindObjectOfType<PlayerManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        spriteRenderer = player.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        // SetTimeAnimation esta en el Update para poder hacer pruebas en la escena,
        // deberia estar en el Start
        SetTimeAnimation();
        FunPlayerAnimation();
    }

    // Es necesario que el numero de fps sea de dos decimales,
    // si no al compararlo en la funcion PlayerAnimation dara error
    private void SetTimeAnimation()
    {
        timeAnimation = (float)decimal.Round(1 / (decimal)playerManager.AnimationFramerate, 2);
    }

    // Transcurrido un periodo de tiempo (timeAnimation), se cambiara el sprite
    private void FunPlayerAnimation()
    {
        time += Time.deltaTime;
        if (time > timeAnimation)
        {
            spriteRenderer.sprite = playerManager.Sprites[i];
            if (i == 2) i = 0;
            else i++;
            time = 0;
        }
    }
}
