using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject Camera;
    private PlayerManager playerManager;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        Vector3 v = playerManager.Player.transform.position;
        v.y +=3f;
        v.z -= 1;
        Camera.transform.position = v;
    }
}
