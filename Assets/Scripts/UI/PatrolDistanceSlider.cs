using UnityEngine;
using UnityEngine.UI;


public class PatrolDistanceSlider : MonoBehaviour
{
    PlayerManager playerManager;
    public Slider slider;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
    }

    public void SetPatrolDistance()
    {
        playerManager.PatrolDistance = (int) slider.value;
    }
}
