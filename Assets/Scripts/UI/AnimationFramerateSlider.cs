using UnityEngine.UI;
using UnityEngine;

public class AnimationFramerateSlider : MonoBehaviour
{
    PlayerManager playerManager;
    public Slider slider;


    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
    }

    public void SetAnimationFramerate()
    {
        playerManager.AnimationFramerate = slider.value;
    }
}
