using System;
using UnityEngine;

public class PatrolMovement : MonoBehaviour
{
    PlayerManager playerManager;
    SpriteRenderer spriteRenderer;
    Vector3 vHor;
    bool patrolIsActive;
    bool turnCheck1;
    bool turnCheck2;
    float time;
    int direction;



    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        spriteRenderer = playerManager.Player.GetComponent<SpriteRenderer>();
        time = 0;
        patrolIsActive = true;
        turnCheck1 = true;
        turnCheck2 = true;
        direction = -1;
    }

    void Update()
    {

        vHor = new Vector3(direction * playerManager.Velocity * Time.deltaTime, 0, 0);
        if (patrolIsActive)
            playerManager.Player.transform.position += vHor;
        else spriteRenderer.sprite = playerManager.Sprites[0];
        
        if (Math.Abs(playerManager.Player.transform.position.x) >= playerManager.PatrolDistance)
        {
            patrolIsActive = false;
            if (!patrolIsActive)
            {
                time += Time.deltaTime;
                if (time >= 1 && turnCheck1)
                {
                    spriteRenderer.flipX = !spriteRenderer.flipX;
                    turnCheck1 = false;
                }
                if (time >= 2 && turnCheck2)
                { 
                    spriteRenderer.flipX = !spriteRenderer.flipX;
                    turnCheck2 = false;
                }
            if (time >= playerManager.SecondsWaiting)
                {
                    spriteRenderer.flipX = !spriteRenderer.flipX;
                    direction = direction * -1;
                    time = 0;
                    patrolIsActive = true;
                    turnCheck1 = true;
                    turnCheck2 = true;
                }
            }
        }

    }
}
