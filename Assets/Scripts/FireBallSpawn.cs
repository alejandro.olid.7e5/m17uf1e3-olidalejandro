using UnityEngine;
public class FireBallSpawn : MonoBehaviour
{
    private GameManager gameManager;
    private int fireballPosition;
    private float counterTime;
    private float timeBetweenFireBalls;


    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        if (counterTime <= 0)
        {
            fireballPosition = Random.Range(-gameManager.FireBallPositionY, gameManager.FireBallPositionY);
            timeBetweenFireBalls = Random.Range(1, gameManager.MaxTimeBetweenFireBalls);
            Spawn(fireballPosition);
            counterTime = timeBetweenFireBalls;
        }
        else
            counterTime -= Time.deltaTime;
    }

    void Spawn(int rnd)
    {
        Vector3 spawnPosition = this.gameObject.transform.position;
        spawnPosition.y = rnd;
        Instantiate(gameManager.Fireball, spawnPosition, Quaternion.identity);
    }
}
